const Koa = require("koa"); // Imports Koa
const logger = require("koa-logger"); // Imports Koa-logger which allows for better error messages
const Router = require("koa-router"); // Imports Koa-router which allows for routing
const mongoose = require("mongoose"); // Imports Mongoose which is used to link to MongoDB
const bodyParser = require("koa-bodyparser"); // Imports Koa-bodyparser which is used to parse requests
const cors = require("@koa/cors");

const session = require("koa-session"); // Imports Koa-session which allows for authentication sessions
const passport = require("koa-passport"); // Imports Koa-passport which allows passport authentication to work with koa

const app = new Koa(); // Creates new Koa app
app.use(cors({ credentials: true }));
const { environment } = require("./config"); // Gets enviroment from config file
console.log(`Your environment is ${environment}`); // Logs environment to console

app.keys = ["super-secret-key"]; // Sets application key (will be secure if used in production)
app.use(session(app)); // Tells app to use sessions

// authentication
require("./auth"); // Fetches auth file functinos
app.use(passport.initialize()); // Intialiases passport authentication
app.use(passport.session()); // Initialises passport sessions

// connect to the database
// needs to be localhost if testing
if (environment != "test") {
  mongoose.connect(`mongodb://database:27017/test`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }); // Connects to database
  var db = mongoose.connection; // Stores connection
  db.on("error", console.error.bind(console, "connection error:")); // Logs any errors
}

app.use(logger()); // Initialises error logging
app.use(bodyParser()); // Initialises request parser

// error handling
// REMOVE FOR PRODUCTION
app.use(async (ctx, next) => {
  try {
    await next();
  } catch (err) {
    ctx.status = err.status || 500; // Sets error status to 500 if the error doesn't already have a code
    ctx.body = err.message; // Adds error message to response
    ctx.app.emit("error", err, ctx); // Emit an error message
  }
});

const router = new Router(); // Create new router

require("./routes")(router); // Require external routes and pass in the router

app.use(router.routes()); // tells router to use all the routes that are on the object
app.use(router.allowedMethods()); // more setup for router

module.exports = app; // exports application (used for testing)

if (environment == "development") app.listen(3000); // tell the server to listen to events on port 3000 if in development
