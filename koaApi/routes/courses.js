const Router = require("koa-router");
const router = new Router();
const Ctrl = require("../controllers/courses");

router.get("/", Ctrl.findAll);
router.post("/", Ctrl.create);
router.get("/:id", Ctrl.findOne);
router.delete("/:id", Ctrl.destroy);
router.post("/:id", Ctrl.update);

module.exports = router.routes();
