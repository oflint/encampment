// Import koa-router
const Router = require("koa-router");
// Create new router for session subroutes
const router = new Router();
// Import session controllers as Ctrl
const Ctrl = require("../controllers/sessions");

// Set routes
router.post("/", Ctrl.create);
router.get("/", Ctrl.findAll);
router.get("/:id", Ctrl.findOne);
router.delete("/:id", Ctrl.destroy);
router.post("/:id", Ctrl.update);
// Export router
module.exports = router.routes();
