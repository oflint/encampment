// Imports
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create new schema for model
const courseSchema = new Schema(
  {
    // Course name
    name: {
      type: String,
      minlength: 1,
      maxlength: 50,
      required: true,
    },
    // Course description
    desc: { type: String, maxlength: 100 },
    // Course specification
    specification: { type: String, maxlength: 100 },
    // Link to user
    user: { type: Schema.Types.ObjectId, ref: "User" },
  },
  // Add timestamps to model
  {
    timestamps: true,
  }
);

// Export model
module.exports = mongoose.model("Course", courseSchema);
