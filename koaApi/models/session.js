// Imports
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const sessionSchema = new Schema(
  {
    // Name of session
    name: {
      type: String,
      minlength: 1,
      maxlength: 50,
      required: true,
    },
    details: { type: String, maxlength: 500 },
    // Stored as time without the :
    duration: { type: Number, min: 1, max: 359999, required: true },
    // Defaults to now
    dateCompleted: { type: Date, default: Date.now },
    // Link to course
    course: { type: Schema.Types.ObjectId, ref: "Course", required: false },
    // Link to user who owns session
    user: { type: Schema.Types.ObjectId, ref: "User" },
  },
  // Add timestamps to model
  {
    timestamps: true,
  }
);

// Export session model
module.exports = mongoose.model("Session", sessionSchema);
