const Course = require("../models/course");
const User = require("../models/user");

// findALl handler
async function findAll(ctx) {
  // Check if authenticated
  if (ctx.isAuthenticated()) {
    // Find all courses belonging to that user
    const courses = await Course.find({ user: ctx.state.user._id });
    // Return courses
    ctx.body = courses;
  } else {
    // Return unauthenticated
    ctx.status = 401;
    ctx.body = { status: "Unauthenticated" };
  }
}
// create handler
async function create(ctx) {
  // check if authenticated
  if (ctx.isAuthenticated()) {
    // create new course document
    const newCourse = new Course(ctx.request.body);
    // set course user id to user id from request
    newCourse.user = ctx.state.user._id;
    // save new course to database and catch any errors
    const savedCourse = await newCourse.save().catch((err) => console.log(err));

    // if course returned, return the course to user
    if (savedCourse) {
      ctx.body = savedCourse;
    } else {
      // if no course returned, return a 400 error
      ctx.status = 400;
    }
  } else {
    // if not authenticated, return a 401 error
    ctx.status = 401;
    ctx.body = { status: "Unauthenticated" };
  }
}

// findOne handler
async function findOne(ctx) {
  // check if authenticated
  if (ctx.isAuthenticated()) {
    // get id from parameters
    const id = ctx.params.id;
    // get course from database
    const course = await Course.findById(id).catch((err) => console.log(err));
    // check course belongs to user
    if (course) {
      if (toString(course.user) === toString(ctx.state.user._id)) {
        // send course back to client
        ctx.body = course;
      } else {
        // return unauthenticated error
        ctx.status = 401;
        ctx.body = { status: "Unauthenticated" };
      }
    } else {
      ctx.status = 400;
    }
  } else {
    // return unauthenticated error
    ctx.status = 401;
    ctx.body = { status: "Unauthenticated" };
  }
}

// Destroy handler
async function destroy(ctx) {
  // If authenticated
  if (ctx.isAuthenticated()) {
    // Get course ID
    const id = ctx.params.id;
    // Attempt to find course
    const course = await Course.findById(id).catch((err) => console.log(err));
    // If course present and user owns the course
    if (course && toString(course.user) == toString(ctx.state.user._id)) {
      // Delete course from database
      const deletedCourse = await course.remove();
      // Return success
      ctx.body = "success";
    } else {
      // Return 400 error
      ctx.status = 400;
    }
  } else {
    ctx.status = 401;
    ctx.body = { status: "Unauthenticated" };
  }
}
async function update(ctx) {
  // If authenticated
  if (ctx.isAuthenticated()) {
    // Get course ID
    const id = ctx.params.id;
    // Attempt to find course
    const course = await Course.findById(id);
    // If course present and user owns the course
    if (course && toString(course.user) == toString(ctx.state.user._id)) {
      try {
        // If new name, set name
        if (ctx.request.body.name) {
          course.name = ctx.request.body.name;
        }
        // If new description, set description
        if (ctx.request.body.desc) {
          course.desc = ctx.request.body.desc;
        }
        // If new specification, set specification
        if (ctx.request.body.specification) {
          course.specification = ctx.request.body.specification;
        }
        // If new user, set user
        if (ctx.request.body.user) {
          console.log(ctx.state.user._id);
          course.user = ctx.request.body.user;
        }

        const savedCourse = await course.save();
        ctx.body = savedCourse;
      } catch (error) {
        ctx.status = 400;
      }
    } else {
      // Return 400 error
      ctx.status = 400;
    }
  } else {
    ctx.status = 401;
    ctx.body = { status: "Unauthenticated" };
  }
}

module.exports = {
  findAll,
  create,
  findOne,
  destroy,
  update,
};
