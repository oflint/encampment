// Require relevant models
const Session = require("../models/session");
const User = require("../models/user");

// findALl handler
async function findAll(ctx) {
  // Check if authenticated
  if (ctx.isAuthenticated()) {
    // Find all sessions belonging to that user
    const sessions = await Session.find({ user: ctx.state.user._id });
    // Return sessions
    ctx.body = sessions;
  } else {
    // Return unauthenticated
    ctx.status = 401;
    ctx.body = { status: "Unauthenticated" };
  }
}

// findOne handler
async function findOne(ctx) {
  // check if authenticated
  if (ctx.isAuthenticated()) {
    // get id from parameters
    const id = ctx.params.id;
    // get session from database
    const session = await Session.findById(id).catch((err) => console.log(err));
    // check session belongs to user
    if (session) {
      if (toString(session.user) === toString(ctx.state.user._id)) {
        // send session back to client
        ctx.body = session;
      } else {
        // return unauthenticated error
        ctx.status = 401;
        ctx.body = { status: "Unauthenticated" };
      }
    } else {
      ctx.status = 400;
    }
  } else {
    // return unauthenticated error
    ctx.status = 401;
    ctx.body = { status: "Unauthenticated" };
  }
}

// create handler
async function create(ctx) {
  // check if authenticated
  if (ctx.isAuthenticated()) {
    // These are here for debugging
    //console.log(ctx.request.body);
    //console.log(ctx.state.user._id);

    // create new session document
    const newSession = new Session(ctx.request.body);
    // set session user id to user id from request
    newSession.user = ctx.state.user._id;
    // save new session to database and catch any errors
    const savedSession = await newSession
      .save()
      .catch((err) => console.log(err));

    // if session returned, return the session to user
    if (savedSession) {
      ctx.body = savedSession;
    } else {
      // if no session returned, return a 400 error
      ctx.status = 400;
    }
  } else {
    // if not authenticated, return a 401 error
    ctx.status = 401;
    ctx.body = { status: "Unauthenticated" };
  }
}

async function destroy(ctx) {
  // If authenticated
  if (ctx.isAuthenticated()) {
    // Get session ID
    const id = ctx.params.id;
    // Attempt to find session
    const session = await Session.findById(id).catch((err) => console.log(err));
    console.log(session);
    // If session present and user owns the session
    if (session && toString(session.user) == toString(ctx.state.user._id)) {
      // Delete session from database
      const deletedSession = await session.remove();
      // Return success
      ctx.body = "success";
    } else {
      // Return 400 error
      ctx.status = 400;
    }
  } else {
    // Retrun 401 unauthenticated
    ctx.status = 401;
    ctx.body = { status: "Unauthenticated" };
  }
}

async function update(ctx) {
  // If authenticated
  if (ctx.isAuthenticated()) {
    // Get session ID
    const id = ctx.params.id;
    // Attempt to find session
    const session = await Session.findById(id);
    // If session present and user owns the session
    if (session && toString(session.user) == toString(ctx.state.user._id)) {
      try {
        // If new name, set name
        if (ctx.request.body.name) {
          session.name = ctx.request.body.name;
        }
        // If new details, set details
        if (ctx.request.body.details) {
          session.details = ctx.request.body.details;
        }
        // If new duration, set duration
        if (ctx.request.body.duration) {
          session.duration = ctx.request.body.duration;
        }
        // If new date completed, set date completed
        if (ctx.request.body.dateCompleted) {
          session.dateCompleted = ctx.request.body.dateCompleted;
        }
        // If new user, set new user
        if (ctx.request.body.user) {
          console.log(ctx.state.user._id);
          session.user = ctx.request.body.user;
        }

        // Save session back to database
        const savedSession = await session.save();
        // Return modified session
        ctx.body = savedSession;
      } catch (error) {
        ctx.status = 400;
      }
    } else {
      // Return 400 error
      ctx.status = 400;
    }
  } else {
    ctx.status = 401;
    ctx.body = { status: "Unauthenticated" };
  }
}

module.exports = {
  create,
  findOne,
  destroy,
  findAll,
  update,
};
