//auth.test.js
const request = require("supertest");
const app = require("../app.js");
const mongoose = require("mongoose");
let User = require("../models/user");

beforeAll(() => {
  // do something before anything else runs
  console.log("Jest starting!");
  // Connect to db
  mongoose.connect(`mongodb://localhost:27017/test`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  mongoose.connection.on(
    "error",
    console.error.bind(console, "connection error:")
  );
  User.remove({}, function (err) {
    console.log("collection removed");
  });
});

// close the server after each test
afterAll(() => {
  //await connection.close();
  mongoose.connection.close();
  console.log("Jest done!");
});

describe("Check authentication status", () => {
  test("Check status GET /api/auth/status", async () => {
    const response = await request(app.callback()).get("/api/auth/status");
    expect(response.status).toEqual(200);
    expect(response.text).toContain("false");
  });
});

describe("Sign up testing", () => {
  test("Valid sign up POST /api/auth/signup", async () => {
    const response = await request(app.callback())
      .post("/api/auth/signup")
      .send({ username: "testing", password: "testing" })
      .set("Accept", "application/json");

    expect(response.status).toEqual(302);
  });
  test("Invalid sign up POST /api/auth/signup", async () => {
    const response = await request(app.callback())
      .post("/api/auth/signup")
      .send({ username: "", password: "testing" })
      .set("Accept", "application/json");

    expect(response.status).toEqual(400);
  });
});

describe("Log out a user", () => {
  test("Log out GET /api/auth/logout", async () => {
    const agent = request.agent(app.callback());
    await agent
      .post("/api/auth/login")
      .send({ username: "testing", password: "testing" })
      .set("Accept", "application/json");

    const response = await agent.get("/api/auth/logout");
    expect(response.status).toEqual(200);
  });
});

describe("Log user in", () => {
  test("Log in POST /api/auth/login", async () => {
    const response = await request(app.callback())
      .post("/api/auth/login")
      .send({ username: "testing", password: "testing" })
      .set("Accept", "application/json");
    expect(response.status).toEqual(302);
  });
});
