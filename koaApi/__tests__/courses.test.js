//courses.test.js
// import required modules
const request = require("supertest");
const app = require("../app.js");
const mongoose = require("mongoose");
const Course = require("../models/course");

// create variable to store the application for testing
let agent;

// before any tests run
beforeAll(async () => {
  // connect to test db
  await mongoose.connect(`mongodb://localhost:27017/test`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  // log error if issue with connection
  mongoose.connection.on(
    "error",
    console.error.bind(console, "connection error:")
  );
});
// Before each test
beforeEach(async () => {
  // Delete all of the courses in the database
  await Course.deleteMany({}, function (err) {
    if (err) console.log(err);
    console.log("Successful deletion");
  });
});

// Create handler course tests
describe("Create routes", () => {
  describe("Unauthenticated routes", () => {
    beforeAll(async () => {
      // using agent allows information to be stored between requests
      // this allows authentication cookies to be preserved
      agent = request.agent(app.callback());
    });
    test("Create unauthenticated POST /api/courses/", async () => {
      // try and post a valid course to the create route
      const response = await agent
        .post("/api/courses/")
        .send({
          name: "testing name",
          desc: "testing details",
          specification: "testing spec",
        })
        .set("Accept", "application/json");
      // expect response status to be 401
      expect(response.status).toEqual(401);
      // log response to console
      // console.log(response.text);
    });
  });

  describe("Authenticated routes", () => {
    beforeAll(async () => {
      // login to application
      // using agent allows information to be stored between requests
      // this allows authentication cookies to be preserved
      agent = request.agent(app.callback());
      // login to test account which exits on database
      // this has already been tested in auth tests
      // any issues will be thrown there
      await agent
        .post("/api/auth/login")
        .send({ username: "testing", password: "test" })
        .set("Accept", "application/json");
    });

    afterAll(async () => {
      await agent.get("/api/auth/logout").set("Accept", "application/json");
    });

    test("Create valid course POST /api/courses/", async () => {
      const response = await agent
        .post("/api/courses/")
        .send({
          name: "testing name",
          desc: "testing desc",
          specification: "testing spec",
        })
        .set("Accept", "application/json");
      expect(response.status).toEqual(200);
      // console.log(response.text);
    });

    test("Create empty course POST /api/courses/", async () => {
      // try and post empty course
      const response = await agent
        .post("/api/courses/")
        .send({})
        .set("Accept", "application/json");
      // expect response status to be 400
      expect(response.status).toEqual(400);
      // expect response status to be 'Bad Request'
      expect(response.text).toEqual("Bad Request");
      // console.log(response.text);
    });

    test("Create invalid course POST /api/courses/", async () => {
      // try and post course with invalid data types
      // server should convert them
      const response = await agent
        .post("/api/courses/")
        .send({
          name: 100,
          desc: 10000,
          specification: 100,
        })
        .set("Accept", "application/json");
      // expect response status to be 200
      expect(response.status).toEqual(200);
      // console.log(response.text);
    });

    test("Create boundary course POST /api/courses/", async () => {
      // try and post course with boundary conditions
      const response = await agent
        .post("/api/courses/")
        .send({
          name: "llllllllllllllllllllllllllllllllllllllllllllllllll",
          desc:
            "llllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll",
          specification:
            "llllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll",
        })
        .set("Accept", "application/json");
      // expect response status to be 200
      expect(response.status).toEqual(200);
      // console.log(response.text);

      // try with one over boundary conditions
      const response2 = await agent
        .post("/api/courses/")
        .send({
          name: "lllllllllllllllllllllllllllllllllllllllllllllllllll",
          desc:
            "lllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll",
          specification:
            "lllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll",
        })
        .set("Accept", "application/json");
      // expect response status to be 400
      expect(response2.status).toEqual(400);
      // console.log(response2.text);
    });
  });
});

// Find One handler course tests
describe("Find One routes", () => {
  beforeAll(async () => {
    // login to application
    // using agent allows information to be stored between requests
    // this allows authentication cookies to be preserved
    agent = request.agent(app.callback());
    // login to test account which exits on database
    // this has already been tested in auth tests
    // any issues will be thrown there
    await agent
      .post("/api/auth/login")
      .send({ username: "testing", password: "test" })
      .set("Accept", "application/json");
  });
  // test finding a valid course
  test("Create and find course GET /api/courses/:id", async () => {
    // create the course to find
    const response = await agent
      .post("/api/courses/")
      .send({
        name: "testing name",
        desc: "testing desc",
        specification: "testing spec",
      })
      .set("Accept", "application/json");

    // get the id of the newly created course
    let id = JSON.parse(response.text)._id;

    // find new course
    const response2 = await agent
      .get("/api/courses/" + id)
      .set("Accept", "application/json");
    console.log(response2.text);
    // should give a 200 response
    expect(response2.status).toEqual(200);
  });
  // test finding an invalid course
  test("Find course with invalid ID GET /api/courses/:id", async () => {
    // try find a course with a random id
    const response = await agent
      .get("/api/courses/5fea13ad6c751c7aabf1239A")
      .set("Accept", "application/json");
    console.log(response.text);
    // should return a 400
    expect(response.status).toEqual(400);
  });
  // try and find a course whilst being logged out
  test("Create and try find course while logged out GET /api/courses/:id", async () => {
    // create course to find
    const response = await agent
      .post("/api/courses/")
      .send({
        name: "testing name",
        desc: "testing desc",
        specification: "testing spec",
      })
      .set("Accept", "application/json");

    // get id of course
    let id = JSON.parse(response.text)._id;
    console.log(id);

    // logout
    await agent.get("/api/auth/logout").set("Accept", "application/json");

    // attempt to find course
    const response2 = await agent
      .get("/api/courses/" + id)
      .set("Accept", "application/json");
    console.log(response2.text);
    // should return unauthenticated error
    expect(response2.status).toEqual(401);
  });
});

// Destroy handler course tests
describe("Destroy routes", () => {
  beforeAll(async () => {
    // login to application
    // using agent allows information to be stored between requests
    // this allows authentication cookies to be preserved
    agent = request.agent(app.callback());
    // login to test account which exits on database
    // this has already been tested in auth tests
    // any issues will be thrown there
    await agent
      .post("/api/auth/login")
      .send({ username: "testing", password: "test" })
      .set("Accept", "application/json");
  });
  // Test normal deletion of a course.
  test("Create and destroy course DELETE /api/courses/:id", async () => {
    // First create a test course
    const response = await agent
      .post("/api/courses/")
      .send({
        name: "testing name",
        desc: "testing desc",
        specification: "testing spec",
      })
      .set("Accept", "application/json");

    // Get newly created course ID
    let id = JSON.parse(response.text)._id;

    // Attempt to delete this course
    const response2 = await agent
      .delete("/api/courses/" + id)
      .set("Accept", "application/json");
    // Expect to be successful
    console.log(response2.text);
    expect(response2.status).toEqual(200);
  });
  // Attempt to destroy a course with an invalid ID
  test("Destroy course with invalid ID DELETE /api/courses/:id", async () => {
    // Attempt to destroy
    const response2 = await agent
      .delete("/api/courses/5fea13ad6c751c7aabf1239A")
      .set("Accept", "application/json");
    console.log(response2.text);
    // Should return 400 error
    expect(response2.status).toEqual(400);
  });
  // Atempt to delete course whilst being logged out
  test("Create and try to delete course while logged out DELETE /api/courses/:id", async () => {
    // Create test course
    const response = await agent
      .post("/api/courses/")
      .send({
        name: "testing name",
        desc: "testing desc",
        specification: "testing spec",
      })
      .set("Accept", "application/json");

    // Get course ID
    let id = JSON.parse(response.text)._id;

    // Logout
    await agent.get("/api/auth/logout").set("Accept", "application/json");

    // Attempt to delete course whilst being logged out
    const response2 = await agent
      .delete("/api/courses/" + id)
      .set("Accept", "application/json");
    console.log(response2.text);
    expect(response2.status).toEqual(401);
  });
});

// Find All handler course tests
describe("Find All routes", () => {
  beforeAll(async () => {
    // login to application
    // using agent allows information to be stored between requests
    // this allows authentication cookies to be preserved
    agent = request.agent(app.callback());
    // login to test account which exits on database
    // this has already been tested in auth tests
    // any issues will be thrown there
    await agent
      .post("/api/auth/login")
      .send({ username: "testing", password: "test" })
      .set("Accept", "application/json");
  });
  // test finding a valid course
  test("Create and find course GET /api/courses/", async () => {
    // create the course to find
    await agent
      .post("/api/courses/")
      .send({
        name: "testing name",
        desc: "testing desc",
        specification: "testing spec",
      })
      .set("Accept", "application/json");

    // find new course
    const response2 = await agent
      .get("/api/courses/")
      .set("Accept", "application/json");
    console.log(response2.text);
    // should give a 200 response
    expect(response2.status).toEqual(200);
    // Expect 1 course to be returned
    expect(JSON.parse(response2.text).length).toEqual(1);
  });
  // test finding multiple courses
  test("Find multiple courses GET /api/courses/", async () => {
    // create two courses to find
    await agent
      .post("/api/courses/")
      .send({
        name: "testing name",
        desc: "testing desc",
        specification: "testing spec",
      })
      .set("Accept", "application/json");
    await agent
      .post("/api/courses/")
      .send({
        name: "testing name",
        desc: "testing desc",
        specification: "testing spec",
      })
      .set("Accept", "application/json");

    // find new courses
    const response2 = await agent
      .get("/api/courses/")
      .set("Accept", "application/json");
    // should give a 200 response
    expect(response2.status).toEqual(200);
    // Expect 2 courses to be returned
    expect(JSON.parse(response2.text).length).toEqual(2);
  });
  // try and find  courses whilst being logged out
  test("Create and try to find courses while logged out GET /api/courses/", async () => {
    // create course to find
    // create two courses to find
    await agent
      .post("/api/courses/")
      .send({
        name: "testing name",
        desc: "testing desc",
        specification: "testing spec",
      })
      .set("Accept", "application/json");
    await agent
      .post("/api/courses/")
      .send({
        name: "testing name",
        desc: "testing desc",
        specification: "testing spec",
      })
      .set("Accept", "application/json");

    // logout
    await agent.get("/api/auth/logout").set("Accept", "application/json");

    // attempt to find course
    const response2 = await agent
      .get("/api/courses/")
      .set("Accept", "application/json");
    console.log(response2.text);
    // should return unauthenticated error
    expect(response2.status).toEqual(401);
  });
});

// Update handler course tests
describe("Update routes", () => {
  beforeAll(async () => {
    // login to application
    // using agent allows information to be stored between requests
    // this allows authentication cookies to be preserved
    agent = request.agent(app.callback());
    // login to test account which exits on database
    // this has already been tested in auth tests
    // any issues will be thrown there
    await agent
      .post("/api/auth/login")
      .send({ username: "testing", password: "test" })
      .set("Accept", "application/json");
  });
  // Test normal updating of a course.
  test("Create and update course POST /api/courses/:id", async () => {
    // First create a test course
    const response = await agent
      .post("/api/courses/")
      .send({
        name: "testing name",
        desc: "testing desc",
        specification: "testing spec",
      })
      .set("Accept", "application/json");

    // Get newly created course ID
    let id = JSON.parse(response.text)._id;

    // Attempt to update this course
    let response2 = await agent
      .post("/api/courses/" + id)
      .set("Accept", "application/json")
      .send({
        name: "testing name change",
        desc: "testing desc change",
        specification: "testing spec change",
      })
      .set("Accept", "application/json");
    // Expect to be successful
    console.log(JSON.parse(response2.text));
    expect(response2.status).toEqual(200);
    expect(JSON.parse(response2.text).name).toEqual("testing name change");
    expect(JSON.parse(response2.text).desc).toEqual("testing desc change");
    expect(JSON.parse(response2.text).specification).toEqual(
      "testing spec change"
    );
  });

  // try and update course so it is invalid
  test("Create and try to update course so it is invalid POST /api/courses/:id", async () => {
    // Create test course
    const response = await agent
      .post("/api/courses/")
      .send({
        name: "testing name",
        desc: "testing desc",
        specification: "testing spec",
      })
      .set("Accept", "application/json");

    // Get course ID
    let id = JSON.parse(response.text)._id;

    // Attempt to update this course so it is invalid
    let response2 = await agent
      .post("/api/courses/" + id)
      .set("Accept", "application/json")
      .send({
        name: "lllllllllllllllllllllllllllllllllllllllllllllllllll",
        desc: "",
        specification: 360000,
      })
      .set("Accept", "application/json");
    console.log(response2.text);
    expect(response2.status).toEqual(400);
  });

  // try and update course whilst being logged out
  test("Create and try to update course while logged out POST /api/courses/:id", async () => {
    // Create test course
    const response = await agent
      .post("/api/courses/")
      .send({
        name: "testing name",
        desc: "testing desc",
        specification: "testing spec",
      })
      .set("Accept", "application/json");

    // Get course ID
    let id = JSON.parse(response.text)._id;

    // Logout
    await agent.get("/api/auth/logout").set("Accept", "application/json");

    // Attempt to update this course
    let response2 = await agent
      .post("/api/courses/" + id)
      .set("Accept", "application/json")
      .send({
        name: "testing name change",
        desc: "testing desc change",
        specification: "testing spec change",
      })
      .set("Accept", "application/json");
    console.log(response2.text);
    // Expect an unauthenticated error
    expect(response2.status).toEqual(401);
  });
});

// close the server after each test
afterAll(() => {
  //close mongoose connection
  mongoose.connection.close();
  console.log("Jest done!");
});
