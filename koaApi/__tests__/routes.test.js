//routes.test.js
const request = require("supertest");
const app = require("../app.js");
const mongoose = require("mongoose");

beforeAll(() => {
  // do something before anything else runs
  console.log("Jest starting!");
  // Connect to db
  mongoose.connect(`mongodb://localhost:27017/test`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  mongoose.connection.on(
    "error",
    console.error.bind(console, "connection error:")
  );
});

// close the server after each test
afterAll(() => {
  //await connection.close();
  mongoose.connection.close();
  console.log("Jest done!");
});

describe("base api tests", () => {
  test("get base api  GET /api", async () => {
    const response = await request(app.callback()).get("/api");
    expect(response.status).toEqual(200);
    expect(response.text).toContain("Hello Api!");
  });
});
describe("base auth tests", () => {
  test("get base account  GET /api/auth", async () => {
    const response = await request(app.callback()).get("/api/auth");
    expect(response.status).toEqual(200);
    expect(response.text).toContain("Hello Auth!");
  });
});
