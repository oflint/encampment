// sessions.test.js
// import required modules
const request = require("supertest");
const app = require("../app.js");
const mongoose = require("mongoose");
const Session = require("../models/session");

// create variable to store the application for testing
let agent;

// before any tests run
beforeAll(async () => {
  // connect to test db
  await mongoose.connect(`mongodb://localhost:27017/test`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  // log error if issue with connection
  mongoose.connection.on(
    "error",
    console.error.bind(console, "connection error:")
  );
});
// Before each test
beforeEach(async () => {
  // Delete all of the sessions in the database
  await Session.deleteMany({}, function (err) {
    if (err) console.log(err);
    console.log("Successful deletion");
  });
});

// Create handler session tests
describe("Create routes", () => {
  describe("Unauthenticated routes", () => {
    beforeAll(async () => {
      // using agent allows information to be stored between requests
      // this allows authentication cookies to be preserved
      agent = request.agent(app.callback());
    });
    test("Create unauthenticated POST /api/sessions/", async () => {
      // try and post a valid session to the create route
      const response = await agent
        .post("/api/sessions/")
        .send({
          name: "testing name",
          details: "testing details",
          duration: 45,
        })
        .set("Accept", "application/json");
      // expect response status to be 401
      expect(response.status).toEqual(401);
      // log response to console
      // console.log(response.text);
    });
  });

  describe("Authenticated routes", () => {
    beforeAll(async () => {
      // login to application
      // using agent allows information to be stored between requests
      // this allows authentication cookies to be preserved
      agent = request.agent(app.callback());
      // login to test account which exits on database
      // this has already been tested in auth tests
      // any issues will be thrown there
      await agent
        .post("/api/auth/login")
        .send({ username: "testing", password: "test" })
        .set("Accept", "application/json");
    });

    afterAll(async () => {
      await agent.get("/api/auth/logout").set("Accept", "application/json");
    });

    test("Create valid session POST /api/sessions/", async () => {
      const response = await agent
        .post("/api/sessions/")
        .send({
          name: "testing name",
          details: "testing desc",
          duration: 1000,
        })
        .set("Accept", "application/json");
      expect(response.status).toEqual(200);
      // console.log(response.text);
    });

    test("Create empty session POST /api/sessions/", async () => {
      // try and post empty session
      const response = await agent
        .post("/api/sessions/")
        .send({})
        .set("Accept", "application/json");
      // expect response status to be 400
      expect(response.status).toEqual(400);
      // expect response status to be 'Bad Request'
      expect(response.text).toEqual("Bad Request");
      // console.log(response.text);
    });

    test("Create invalid session POST /api/sessions/", async () => {
      // try and post session with invalid data types
      // server should convert them
      const response = await agent
        .post("/api/sessions/")
        .send({
          name: 100,
          details: 10000,
          duration: "100",
        })
        .set("Accept", "application/json");
      // expect response status to be 200
      expect(response.status).toEqual(200);
      // console.log(response.text);

      // server should reject this data
      const response2 = await agent
        .post("/api/sessions/")
        .send({
          name: 100,
          details: 10000,
          duration: "test",
        })
        .set("Accept", "application/json");
      // expect response status to be 400
      expect(response2.status).toEqual(400);
      // console.log(response2.text);
    });

    test("Create boundary session POST /api/sessions/", async () => {
      // try and post session with boundary conditions
      const response = await agent
        .post("/api/sessions/")
        .send({
          name: "llllllllllllllllllllllllllllllllllllllllllllllllll",
          details:
            "llllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll",
          duration: 359999,
        })
        .set("Accept", "application/json");
      // expect response status to be 200
      expect(response.status).toEqual(200);
      // console.log(response.text);

      // try with one over boundary conditions
      const response2 = await agent
        .post("/api/sessions/")
        .send({
          name: "lllllllllllllllllllllllllllllllllllllllllllllllllll",
          details:
            "lllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll",
          duration: 360000,
        })
        .set("Accept", "application/json");
      // expect response status to be 400
      expect(response2.status).toEqual(400);
      // console.log(response2.text);
    });
  });
});

// Find One handler session tests
describe("Find One routes", () => {
  beforeAll(async () => {
    // login to application
    // using agent allows information to be stored between requests
    // this allows authentication cookies to be preserved
    agent = request.agent(app.callback());
    // login to test account which exits on database
    // this has already been tested in auth tests
    // any issues will be thrown there
    await agent
      .post("/api/auth/login")
      .send({ username: "testing", password: "test" })
      .set("Accept", "application/json");
  });
  // test finding a valid session
  test("Create and find session GET /api/sessions/:id", async () => {
    // create the session to find
    const response = await agent
      .post("/api/sessions/")
      .send({
        name: "testing name",
        details: "testing desc",
        duration: 1000,
      })
      .set("Accept", "application/json");

    // get the id of the newly created session
    let id = JSON.parse(response.text)._id;

    // find new session
    const response2 = await agent
      .get("/api/sessions/" + id)
      .set("Accept", "application/json");
    console.log(response2.text);
    // should give a 200 response
    expect(response2.status).toEqual(200);
  });
  // test finding an invalid session
  test("Find session with invalid ID GET /api/sessions/:id", async () => {
    // try find a session with a random id
    const response = await agent
      .get("/api/sessions/5fea13ad6c751c7aabf1239A")
      .set("Accept", "application/json");
    console.log(response.text);
    // should return a 400
    expect(response.status).toEqual(400);
  });
  // try and find a session whilst being logged out
  test("Create and try find session while logged out GET /api/sessions/:id", async () => {
    // create session to find
    const response = await agent
      .post("/api/sessions/")
      .send({
        name: "testing name",
        details: "testing desc",
        duration: 1000,
      })
      .set("Accept", "application/json");

    // get id of session
    let id = JSON.parse(response.text)._id;
    console.log(id);

    // logout
    await agent.get("/api/auth/logout").set("Accept", "application/json");

    // attempt to find session
    const response2 = await agent
      .get("/api/sessions/" + id)
      .set("Accept", "application/json");
    console.log(response2.text);
    // should return unauthenticated error
    expect(response2.status).toEqual(401);
  });
});

// Destroy handler session tests
describe("Destroy routes", () => {
  beforeAll(async () => {
    // login to application
    // using agent allows information to be stored between requests
    // this allows authentication cookies to be preserved
    agent = request.agent(app.callback());
    // login to test account which exits on database
    // this has already been tested in auth tests
    // any issues will be thrown there
    await agent
      .post("/api/auth/login")
      .send({ username: "testing", password: "test" })
      .set("Accept", "application/json");
  });
  // Test normal deletion of a session.
  test("Create and destroy session DELETE /api/sessions/:id", async () => {
    // First create a test session
    const response = await agent
      .post("/api/sessions/")
      .send({
        name: "testing name",
        details: "testing desc",
        duration: 1000,
      })
      .set("Accept", "application/json");

    // Get newly created session ID
    let id = JSON.parse(response.text)._id;

    // Attempt to delete this session
    const response2 = await agent
      .delete("/api/sessions/" + id)
      .set("Accept", "application/json");
    // Expect to be successful
    console.log(response2.text);
    expect(response2.status).toEqual(200);
  });
  // Attempt to destroy a session with an invalid ID
  test("Destroy session with invalid ID DELETE /api/sessions/:id", async () => {
    // Attempt to destroy
    const response2 = await agent
      .delete("/api/sessions/5fea13ad6c751c7aabf1239A")
      .set("Accept", "application/json");
    console.log(response2.text);
    // Should return 400 error
    expect(response2.status).toEqual(400);
  });
  // Atempt to delete session whilst being logged out
  test("Create and try to delete session while logged out DELETE /api/sessions/:id", async () => {
    // Create test session
    const response = await agent
      .post("/api/sessions/")
      .send({
        name: "testing name",
        details: "testing desc",
        duration: 1000,
      })
      .set("Accept", "application/json");

    // Get session ID
    let id = JSON.parse(response.text)._id;

    // Logout
    await agent.get("/api/auth/logout").set("Accept", "application/json");

    // Attempt to delete session whilst being logged out
    const response2 = await agent
      .delete("/api/sessions/" + id)
      .set("Accept", "application/json");
    console.log(response2.text);
    expect(response2.status).toEqual(401);
  });
});

// Find All handler session tests
describe("Find All routes", () => {
  beforeAll(async () => {
    // login to application
    // using agent allows information to be stored between requests
    // this allows authentication cookies to be preserved
    agent = request.agent(app.callback());
    // login to test account which exits on database
    // this has already been tested in auth tests
    // any issues will be thrown there
    await agent
      .post("/api/auth/login")
      .send({ username: "testing", password: "test" })
      .set("Accept", "application/json");
  });
  // test finding a valid session
  test("Create and find session GET /api/sessions/", async () => {
    // create the session to find
    await agent
      .post("/api/sessions/")
      .send({
        name: "testing name",
        details: "testing desc",
        duration: 1000,
      })
      .set("Accept", "application/json");

    // find new session
    const response2 = await agent
      .get("/api/sessions/")
      .set("Accept", "application/json");
    console.log(response2.text);
    // should give a 200 response
    expect(response2.status).toEqual(200);
    // Expect 1 session to be returned
    expect(JSON.parse(response2.text).length).toEqual(1);
  });
  // test finding multiple sessions
  test("Find multiple sessions GET /api/sessions/", async () => {
    // create two sessions to find
    await agent
      .post("/api/sessions/")
      .send({
        name: "testing name",
        details: "testing desc",
        duration: 1000,
      })
      .set("Accept", "application/json");
    await agent
      .post("/api/sessions/")
      .send({
        name: "testing name2",
        details: "testing desc2",
        duration: 2000,
      })
      .set("Accept", "application/json");

    // find new sessions
    const response2 = await agent
      .get("/api/sessions/")
      .set("Accept", "application/json");
    // should give a 200 response
    expect(response2.status).toEqual(200);
    // Expect 2 sessions to be returned
    expect(JSON.parse(response2.text).length).toEqual(2);
  });
  // try and find  sessions whilst being logged out
  test("Create and try to find sessions while logged out GET /api/sessions/", async () => {
    // create session to find
    // create two sessions to find
    await agent
      .post("/api/sessions/")
      .send({
        name: "testing name",
        details: "testing desc",
        duration: 1000,
      })
      .set("Accept", "application/json");
    await agent
      .post("/api/sessions/")
      .send({
        name: "testing name2",
        details: "testing desc2",
        duration: 2000,
      })
      .set("Accept", "application/json");

    // logout
    await agent.get("/api/auth/logout").set("Accept", "application/json");

    // attempt to find session
    const response2 = await agent
      .get("/api/sessions/")
      .set("Accept", "application/json");
    console.log(response2.text);
    // should return unauthenticated error
    expect(response2.status).toEqual(401);
  });
});

// Update handler session tests
describe("Update routes", () => {
  beforeAll(async () => {
    // login to application
    // using agent allows information to be stored between requests
    // this allows authentication cookies to be preserved
    agent = request.agent(app.callback());
    // login to test account which exits on database
    // this has already been tested in auth tests
    // any issues will be thrown there
    await agent
      .post("/api/auth/login")
      .send({ username: "testing", password: "test" })
      .set("Accept", "application/json");
  });
  // Test normal updating of a session.
  test("Create and update session POST /api/sessions/:id", async () => {
    // First create a test session
    const response = await agent
      .post("/api/sessions/")
      .send({
        name: "testing name",
        details: "testing desc",
        duration: 1000,
      })
      .set("Accept", "application/json");

    // Get newly created session ID
    let id = JSON.parse(response.text)._id;

    // Attempt to update this session
    let response2 = await agent
      .post("/api/sessions/" + id)
      .set("Accept", "application/json")
      .send({
        name: "testing changing the name",
        details: "testing desc change",
        duration: 2000,
      })
      .set("Accept", "application/json");
    // Expect to be successful
    console.log(JSON.parse(response2.text));
    expect(response2.status).toEqual(200);
    expect(JSON.parse(response2.text).name).toEqual(
      "testing changing the name"
    );
    expect(JSON.parse(response2.text).details).toEqual("testing desc change");
    expect(JSON.parse(response2.text).duration).toEqual(2000);
  });

  // try and update session so it is invalid
  test("Create and try to update session so it is invalid POST /api/sessions/:id", async () => {
    // Create test session
    const response = await agent
      .post("/api/sessions/")
      .send({
        name: "testing name",
        details: "testing desc",
        duration: 1000,
      })
      .set("Accept", "application/json");

    // Get session ID
    let id = JSON.parse(response.text)._id;

    // Attempt to update this session so it is invalid
    let response2 = await agent
      .post("/api/sessions/" + id)
      .set("Accept", "application/json")
      .send({
        name: "lllllllllllllllllllllllllllllllllllllllllllllllllll",
        details: "",
        duration: 360000,
      })
      .set("Accept", "application/json");
    console.log(response2.text);
    expect(response2.status).toEqual(400);
  });

  // try and update session whilst being logged out
  test("Create and try to update session while logged out POST /api/sessions/:id", async () => {
    // Create test session
    const response = await agent
      .post("/api/sessions/")
      .send({
        name: "testing name",
        details: "testing desc",
        duration: 1000,
      })
      .set("Accept", "application/json");

    // Get session ID
    let id = JSON.parse(response.text)._id;

    // Logout
    await agent.get("/api/auth/logout").set("Accept", "application/json");

    // Attempt to update this session
    let response2 = await agent
      .post("/api/sessions/" + id)
      .set("Accept", "application/json")
      .send({
        name: "testing changing the name",
        details: "testing desc change",
        duration: 2000,
      })
      .set("Accept", "application/json");
    console.log(response2.text);
    // Expect an unauthenticated error
    expect(response2.status).toEqual(401);
  });
});

// close the server after each test
afterAll(() => {
  //close mongoose connection
  mongoose.connection.close();
  console.log("Jest done!");
});
