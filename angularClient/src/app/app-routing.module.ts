import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CoursesComponent } from "./courses/courses.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { SessionsComponent } from "./sessions/sessions.component";
import { SettingsComponent } from "./settings/settings.component";
import { StatisticsComponent } from "./statistics/statistics.component";
import { AuthGuard } from "./guards/auth.guard";
import { LoginComponent } from "./auth/login/login.component";
import { SignupComponent } from "./auth/signup/signup.component";
import { SessionDetailComponent } from "./detail/session-detail/session-detail.component";

const routes: Routes = [
  { path: "", redirectTo: "/dashboard", pathMatch: "full" },
  {
    path: "dashboard",
    component: DashboardComponent,
    canActivate: [AuthGuard],
  },
  { path: "courses", component: CoursesComponent, canActivate: [AuthGuard] },
  { path: "sessions", component: SessionsComponent, canActivate: [AuthGuard] },
  {
    path: "sessions/:id",
    component: SessionDetailComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "statistics",
    component: StatisticsComponent,
    canActivate: [AuthGuard],
  },
  { path: "settings", component: SettingsComponent, canActivate: [AuthGuard] },
  {
    path: "login",
    component: LoginComponent,
  },
  {
    path: "signup",
    component: SignupComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
