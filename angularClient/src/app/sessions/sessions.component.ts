import { Component, OnInit } from "@angular/core";
import { DataService } from "../services/data.service";

@Component({
  selector: "app-sessions",
  templateUrl: "./sessions.component.html",
  styleUrls: ["./sessions.component.scss"],
})
export class SessionsComponent implements OnInit {
  // Session storage variable
  sessions = [];
  constructor(private data: DataService) {}

  // When component is initialised
  async ngOnInit() {
    // Get sessions from server
    this.sessions = await this.data.getAllSessions();
    // Sort sessions so that most recent is first
    this.sessions.sort((a, b) => {
      let tempA = new Date(a.dateCompleted);
      let tempB = new Date(b.dateCompleted);
      if (tempA > tempB) {
        return -1;
      } else if (tempB > tempA) {
        return 1;
      } else {
        return 0;
      }
    });
  }
}
