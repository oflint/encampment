import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthService } from "src/app/services/auth.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  public user = {
    username: null,
    password: null,
  };

  constructor(private auth: AuthService, private router: Router) {}

  async onSubmit(form: NgForm) {
    await this.auth.login(this.user.username, this.user.password);
    if (this.auth.isLoggedIn()) {
      this.router.navigate(["/dashboard"]);
      form.resetForm();
    } else {
      form.resetForm();
    }
  }

  signup() {
    // Navigate to signup page
    this.router.navigate(["/signup"]);
  }
  ngOnInit(): void {}
}
