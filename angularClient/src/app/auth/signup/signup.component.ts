import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthService } from "src/app/services/auth.service";

@Component({
  selector: "app-signup",
  templateUrl: "./signup.component.html",
  styleUrls: ["./signup.component.scss"],
})
export class SignupComponent implements OnInit {
  public user = {
    username: null,
    password: null,
  };
  constructor(private auth: AuthService, private router: Router) {}

  ngOnInit(): void {}

  async onSubmit(form: NgForm) {
    await this.auth.signup(this.user.username, this.user.password);
    if (this.auth.isLoggedIn()) {
      this.router.navigate(["/dashboard"]);
      form.resetForm();
    } else {
      form.resetForm();
    }
  }

  login() {
    // Take user back to login page
    this.router.navigate(["/login"]);
  }
}
