import { Injectable } from "@angular/core";
import { Router } from "@angular/router";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  constructor(private router: Router) {
    // Get info from localstorage to see if user is logged in
    this.loggedIn = localStorage.getItem("loggedIn") == "true";
    console.log(this.loggedIn);
    fetch("http://localhost:3000/api/auth/status/", {
      credentials: "include",
      method: "GET",
    })
      .then((response) => response.text())
      .then((data) => {
        console.log("islogged in", data);
        if (data == "false" && this.loggedIn == true) {
          this.loggedIn = false;
          localStorage.getItem("loggedIn") == "false";
          this.router.navigate(["/login"]);
        }
      });
  }
  // For checking whether user is logged in
  private loggedIn: boolean;

  isLoggedIn() {
    // Log to console for debugging
    console.log(this.loggedIn);
    // Return whether user is logged in
    return this.loggedIn;
  }

  // Log user into system
  async login(username: string, password: string) {
    // Attempt to login
    await fetch("http://localhost:3000/api/auth/login/", {
      credentials: "include",
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ username: username, password: password }),
    })
      .then((response) => response.text())
      .then((data) => {
        try {
          if (data == "true") {
            // Set logged in to true
            this.loggedIn = true;
            // Set localstorage value
            localStorage.setItem("loggedIn", "true");
          }
        } catch (error) {
          console.log(error);
        }
      });
  }

  // Log user out of system
  async logout() {
    await fetch("http://localhost:3000/api/auth/logout/", {
      credentials: "include",
      method: "GET",
    }).then((response) => {
      // Set logged in to false
      this.loggedIn = false;
      localStorage.setItem("loggedIn", "false");
    });
  }

  async signup(username: string, password: string) {
    // Attempt to signup user. If successful, user will be logged in
    await fetch("http://localhost:3000/api/auth/signup/", {
      credentials: "include",
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ username: username, password: password }),
    })
      .then((response) => response.text())
      .then((data) => {
        try {
          if (data == "true") {
            // Set logged in to true
            this.loggedIn = true;
            // Set localstorage value
            localStorage.setItem("loggedIn", "true");
          } else {
          }
        } catch (error) {
          console.log(error);
        }
      });
  }
}
