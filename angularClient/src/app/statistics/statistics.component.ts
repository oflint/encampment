import { Component, OnInit } from "@angular/core";
import { DataService } from "../services/data.service";
import { DurationPipe } from "../pipes/duration.pipe";
import * as moment from "moment";

@Component({
  selector: "app-statistics",
  templateUrl: "./statistics.component.html",
  styleUrls: ["./statistics.component.scss"],
})
export class StatisticsComponent implements OnInit {
  // Session storage variable
  sessions = [];
  sessionsToday = [];
  timeCompleted = 0;
  timeCompletedString = "";
  percentageComplete = 0;
  constructor(private data: DataService, private duration: DurationPipe) {}

  // When component is initialised
  async ngOnInit() {
    // Get sessions from server
    this.sessions = await this.data.getAllSessions();
    // Sort sessions so that most recent is first
    this.sessions.sort((a, b) => {
      let tempA = new Date(a.dateCompleted);
      let tempB = new Date(b.dateCompleted);
      if (tempA > tempB) {
        return -1;
      } else if (tempB > tempA) {
        return 1;
      } else {
        return 0;
      }
    });

    this.sessionsToday = this.sessions.filter((session) => {
      console.log(
        new Date(session.dateCompleted),
        new Date(Date.now()),
        new Date(Date.now()).toDateString()
      );
      return (
        new Date(session.dateCompleted).toDateString() ==
        new Date(Date.now()).toDateString()
      );
    });
    console.log("sessions today", this.sessionsToday);

    let secondsCompleted = 0;
    let minutesCompleted = 0;
    let hoursCompleted = 0;
    this.sessionsToday.forEach((session) => {
      let tempString = session.duration.toString().split("");
      let zerosToAdd = 6 - tempString.length;
      if (tempString.length < 6) {
        for (let i = 0; i < zerosToAdd; i++) {
          tempString.unshift("0");
        }
      }
      tempString = tempString.join("");
      hoursCompleted += parseInt(tempString.slice(0, 2));
      minutesCompleted += parseInt(tempString.slice(2, 4));
      secondsCompleted += parseInt(tempString.slice(4, 6));
    });
    console.log(this.duration.transform(this.timeCompleted.toString()));
    if (secondsCompleted >= 60) {
      minutesCompleted += Math.floor(secondsCompleted / 60);
      secondsCompleted = secondsCompleted % 60;
    }
    if (minutesCompleted >= 60) {
      hoursCompleted += Math.floor(minutesCompleted / 60);
      minutesCompleted = minutesCompleted % 60;
    }
    let tempList = [
      hoursCompleted.toString(),
      minutesCompleted.toString(),
      secondsCompleted.toString(),
    ];
    tempList.forEach((item) => {
      if (item.length == 1) {
        let tempItem = item.split("");
        tempItem.unshift("0");
        let tempString = tempItem.join("");
        tempList[tempList.indexOf(item)] = tempString;
      }
    });
    let percentageList = [hoursCompleted.toString()];
    console.log(tempList);
    percentageList[1] = Math.round(
      (parseInt(tempList[1]) / 60) * 100
    ).toString();
    percentageList[2] = Math.round(
      (parseInt(tempList[2]) / 60) * 100
    ).toString();
    percentageList.forEach((item) => {
      console.log("testing percentage lists", item, item.length);
      if (item.length == 1) {
        let tempItem = item.split("");
        tempItem.unshift("0");
        let tempString = tempItem.join("");
        percentageList[percentageList.indexOf(item)] = tempString;
      }
    });
    this.timeCompleted = parseInt(tempList.join(""));

    console.log("time completed", this.timeCompleted);
    console.log("time completed today", this.timeCompleted);
    console.log("percentage list", percentageList.join(""));
    this.percentageComplete = Math.floor(
      (parseInt(percentageList.join("")) / 30000) * 100
    );
    console.log(this.percentageComplete);
    this.move(this.percentageComplete);
    this.timeCompletedString = this.timeCompleted.toString();
  }

  private i = 0;
  move(moveTo) {
    if (this.i == 0) {
      let maxVal = moveTo;
      this.i = 1;
      var elem = document.getElementById("progressBar");
      var width = 0;
      var id = setInterval(function () {
        frame(maxVal);
      }, 1);
      function frame(maxVal) {
        if (width >= maxVal) {
          clearInterval(id);
          this.i = 0;
        } else if (width == 100) {
          elem.innerHTML = maxVal + "%";
          clearInterval(id);
          this.i = 0;
        } else {
          width++;
          elem.style.width = width + "%";
          elem.innerHTML = width + "%";
        }
      }
    }
  }

  getWeekSessions() {
    let currentDate = moment();
    let filtered = this.sessions.filter((date) =>
      moment(date).isSame(currentDate, "week")
    );
    console.log(filtered);
  }
}

/*
var currentDate = moment();
var allDates = [...];
var filtered = allDates.filter(date => moment(date).isSame(currentDate, 'week');
*/
