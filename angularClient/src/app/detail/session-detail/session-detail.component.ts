import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NgForm } from "@angular/forms";
import { DurationPipe } from "../../pipes/duration.pipe";

@Component({
  selector: "app-session-detail",
  templateUrl: "./session-detail.component.html",
  styleUrls: ["./session-detail.component.scss"],
})
export class SessionDetailComponent implements OnInit {
  id;
  public tempSession = {
    name: null,
    details: null,
    duration: null,
    course: null,
  };
  session;
  constructor(
    private route: ActivatedRoute,
    private durationPipe: DurationPipe,
    private router: Router
  ) {}

  // On initialisation
  async ngOnInit() {
    // Get session ID
    this.id = this.route.snapshot.paramMap.get("id");
    // Fetch session information
    this.session = await fetch(
      "http://localhost:3000/api/sessions/" + this.id,
      {
        credentials: "include",
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      }
    )
      .then((response) => response.json())
      .then((data) => {
        return data;
      });
    // Update form with information
    this.tempSession.name = this.session.name;
    this.tempSession.details = this.session.details;
    // Use the duration pipe to display correctly
    this.tempSession.duration = this.durationPipe.transform(
      this.session.duration
    );
    this.tempSession.course = this.session.course;
  }

  // Delete function
  async delete() {
    //Send a delete request
    await fetch("http://localhost:3000/api/sessions/" + this.id + "/", {
      credentials: "include",
      method: "DELETE",
    })
      .then((response) => response.text())
      .then((data) => {
        // Attempt to log response and navigate to session log
        try {
          console.log(data);
          this.router.navigate(["/sessions"]);
        } catch (error) {
          console.log(error);
        }
      });
  }

  // On submission of form
  async onSubmit(form: NgForm) {
    // Get an integer for duration
    let duration = parseInt(this.tempSession.duration.replace(/:/g, ""));
    // Submit updated session
    await fetch("http://localhost:3000/api/sessions/" + this.id + "/", {
      credentials: "include",
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name: this.tempSession.name,
        details: this.tempSession.details,
        duration: duration,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        // Attempt to log data and navigate to session log
        try {
          console.log(data);
          this.router.navigate(["/sessions"]);
        } catch (error) {
          console.log(error);
        }
      });
  }
}
