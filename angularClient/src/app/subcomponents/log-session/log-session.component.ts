import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { AuthService } from "../../services/auth.service";

@Component({
  selector: "app-log-session",
  templateUrl: "./log-session.component.html",
  styleUrls: ["./log-session.component.scss"],
})
export class LogSessionComponent implements OnInit {
  public session = {
    name: null,
    details: null,
    duration: null,
    course: null,
  };
  constructor(private Auth: AuthService) {}

  async ngOnInit() {
    /*
    await fetch("http://localhost:3000/api/auth/status/", {
      credentials: "include",
    })
      .then((response) => response.text())
      .then((data) => console.log(data));
    console.log(await this.Auth.isLoggedIn());
    if (!(await this.Auth.isLoggedIn())) await this.Auth.login("test", "test");
    console.log(await this.Auth.isLoggedIn());
    */
  }
  async onSubmit(form: NgForm) {
    // Get duration in required format
    let duration = parseInt(this.session.duration.replace(/:/g, ""));

    // Send details to server
    await fetch("http://localhost:3000/api/sessions/", {
      credentials: "include",
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name: this.session.name,
        details: this.session.details,
        duration: duration,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        // Attempt to log data and reset form
        try {
          console.log(data);
          form.resetForm();
        } catch (error) {
          console.log(error);
        }
      });
  }
}
