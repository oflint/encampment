import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "duration",
})
export class DurationPipe implements PipeTransform {
  transform(stringInput: String): unknown {
    let tempString = stringInput.toString().split("");
    let zerosToAdd = 6 - tempString.length;
    if (tempString.length < 6) {
      for (let i = 0; i < zerosToAdd; i++) {
        tempString.unshift("0");
      }
    }
    let temp: string[] = [];
    temp.push(tempString[0]);
    temp.push(tempString[1]);
    temp.push(":");
    temp.push(tempString[2]);
    temp.push(tempString[3]);
    temp.push(":");
    temp.push(tempString[4]);
    temp.push(tempString[5]);
    return temp.join("");
  }
}
