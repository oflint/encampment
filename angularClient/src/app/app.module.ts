import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HttpClientModule } from "@angular/common/http";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { PastSessionsComponent } from "./subcomponents/past-sessions/past-sessions.component";
import { PastCoursesComponent } from "./subcomponents/past-courses/past-courses.component";
import { CoursesComponent } from "./courses/courses.component";
import { SessionsComponent } from "./sessions/sessions.component";
import { StatisticsComponent } from "./statistics/statistics.component";
import { SettingsComponent } from "./settings/settings.component";
import { DashStatisticsComponent } from "./subcomponents/dash-statistics/dash-statistics.component";
import { LogSessionComponent } from "./subcomponents/log-session/log-session.component";
import { LoginComponent } from "./auth/login/login.component";
import { SignupComponent } from "./auth/signup/signup.component";
import { FormsModule } from "@angular/forms";
import { DurationPipe } from "./pipes/duration.pipe";
import { SessionDetailComponent } from "./detail/session-detail/session-detail.component";

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    CoursesComponent,
    SessionsComponent,
    SettingsComponent,
    StatisticsComponent,
    PastSessionsComponent,
    PastCoursesComponent,
    DashStatisticsComponent,
    LogSessionComponent,
    LoginComponent,
    SignupComponent,
    DurationPipe,
    SessionDetailComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, FormsModule],
  providers: [DurationPipe],
  bootstrap: [AppComponent],
})
export class AppModule {}
